import 'dart:math';
import 'package:intro_to_dart_for_java_developers/src/bicycle.dart';
import 'package:intro_to_dart_for_java_developers/src/shapes/rectangle.dart';
import 'package:intro_to_dart_for_java_developers/src/shapes/square.dart';
import 'package:intro_to_dart_for_java_developers/src/shapes/circle.dart';

void main() {
  printDebugConsole('Bicycle', useBicycle);
  printDebugConsole('Rectangle', showRectangle);
  printDebugConsole('Square', showSquareArea);
  printDebugConsole('Circle', showCircleArea);
}

void showCircleArea() {
  final circle = Circle(2);
  print(circle);
}

void showSquareArea() {
  final square = Square(2);
  print(square);
}

void showRectangle() {
  print(Rectangle(origin: const Point(10, 20), width: 100, height: 200));
  print(Rectangle(origin: const Point(10, 10)));
  print(Rectangle(width: 200));
  print(Rectangle());
}

void useBicycle() {
  var bike = Bicycle();

  print('Starting bike...');
  print(bike);

  print('Speeding up...');
  bike.speedUp(80);
  print(bike);

  print('Applying brake...');
  bike.applyBrake(20);
  print(bike);
}

void printDebugConsole(String title, Function f) {
  print('----------- $title -----------');;
  f();
  print('----------- $title -----------');
  print('\n');
}
