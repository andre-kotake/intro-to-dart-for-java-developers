import 'dart:math';
import 'package:intro_to_dart_for_java_developers/src/shape.dart';

class Rectangle implements Shape {
  int _width;
  int get width => _width;
  int _height;
  int get height => _height;
  Point _origin;
  Point get origin => _origin;

  Rectangle({Point origin = const Point(0, 0), int width = 0, height = 0}) {
    _width = width;
    _height = height;
    _origin = origin;
  }

  @override
  String toString() =>
      'Origin: (${_origin.x}, ${_origin.y}), width: $_width, height: $_height, area: $area';

  @override
  num get area => (width * height);
}
