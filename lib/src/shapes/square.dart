import 'dart:math';
import 'package:intro_to_dart_for_java_developers/src/shape.dart';

class Square implements Shape {
  final num _side;
  num get side => _side;

  Square(this._side);

  @override
  num get area => pow(_side, 2);

  @override
  String toString() => 'side: $_side, area: $area';
}
