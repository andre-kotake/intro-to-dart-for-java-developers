import 'dart:math';
import 'package:intro_to_dart_for_java_developers/src/shape.dart';

class Circle implements Shape {
  final num _radius;
  num get radius => _radius;

  Circle(this._radius);

  @override
  num get area => pi * pow(_radius, 2);

  @override
  String toString() => 'radius: $_radius, area: $area';
}
