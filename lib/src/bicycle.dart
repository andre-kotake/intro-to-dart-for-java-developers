class Bicycle {
  int _speed = 0;
  int get speed => _speed;

  Bicycle();

  void applyBrake(int decrement) => _speed -= decrement;

  void speedUp(int increment) => _speed += increment;

  @override
  String toString() => 'Bicycle speed: $_speed km/h';
}
